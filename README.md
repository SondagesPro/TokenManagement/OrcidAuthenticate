# OrcidAuthenticate

Allow to use Oauth by Orcid for survey register.

This plugin need PHP 7.2.5 minimum

## Usage

### Register and set global settings of plugin

- Choose your Register URI type on plugin settings
- Copy the url
- Register your application on [sondbox](https://orcid.org/content/register-client-application-sandbox) or [final api](https://info.orcid.org/register-a-client-application-production-member-api/) using this url for Redirect URIs
- Enter the Client ID and Client secret in your settings
- Check the other settings

### Use orcid authentication on one survey

- Activate token table, partcipant table
- Set _Allow public registration:_ survey settings to on
- Check specific survey settings at tool menu.
- You can choose to force or not Authenatication with Orcid to enter teh esurvey, if you leave _Force orcid authenticate_ to Automatic : your protect the data of Orcid user.

## Home page & Copyright
- Copyright © 2020-2021 Denis Chenu <http://sondages.pro>
- Copyright © 2020-2021 OECD <http://www.oecd.org>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
