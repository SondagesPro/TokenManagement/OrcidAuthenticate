function orcidLogout(orcidLogOutUrl) {
    $.ajax({
        url: orcidLogOutUrl,
        dataType: 'jsonp',
        success: function(result,status,xhr) {
            // OK
        },
        error: function (xhr, status, error) {
            // KO
        }
    });
}
