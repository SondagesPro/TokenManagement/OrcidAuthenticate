��          t       �       �      �      �      �                8     K     \  !   q  �  �  �   :     9  
   L     W     r  %   �     �     �     �  2   �  �     Default introduction Introduction Introduction text in %s (%s) ORCID iD icon Orcid Authentication settings Orcid Introduction Orcid activation Orcid authentication Register or Connect your ORCID iD When you click the “Authorize” button, we will ask you to share your iD using an authenticated process: either by %sregistering for an ORCID iD%s or, if you already have one, by %ssigning into your ORCID account%s , then granting us permission to get your ORCID iD. We do this to ensure that you are correctly identified and securely connecting your ORCID iD. Learn more about %sWhat’s so special about signing in%s. PO-Revision-Date: 2020-10-02 08:21:52+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.1
Language: de
Project-Id-Version: OrcidAuthenticate
 Standardeinleitung Einleitung Einleitungstext in %s (%s) ORCID iD Symbol ORCID Authentifizierungseinstellungen ORCID Einleitung ORCID Aktivierung ORCID Authentifizierung Registriere oder verbinde Dich mit Deiner ORCID iD Wenn Du auf die Schaltfläche "Autorisieren" klickst, wirst Du aufgefordert, Deine iD mithilfe eines authentifizierten Prozesses freizugeben: entweder durch %sregistering for an ORCID iD%s oder, falls Du bereits eine hast, durch %ssigning into your ORCID account%s, Erteile uns dann die Erlaubnis, Deine ORCID iD zu erhalten. Wir tun dies, um sicherzustellen, dass Du korrekt identifiziert bist und Deine ORCID iD sicher verbunden ist. Erfahre mehr über %sWhat’s so special about signing in%s. 