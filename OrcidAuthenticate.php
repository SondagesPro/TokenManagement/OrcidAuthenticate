<?php
/**
 * ORCID () authentification for registering
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2022 Denis Chenu <http://www.sondages.pro>
 * @copyright 2020-2021 OECD <http://www.oecd.org>
 * @license AGPL v3
 * @version 0.8.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class OrcidAuthenticate extends PluginBase {

    protected $storage = 'DbStorage';

    static protected $description = 'ORCID authentification for registering.';
    static protected $name = 'OrcidAuthenticate';

    /** Object \League\OAuth2\Client\Provider\GenericProvider */
    private $provider;

    protected $settings = array(
        'SandBox' => array(
            'type' => 'boolean',
            'label' => 'use orcid sandbox',
            'default' => 0,
            'help' => "Before move to Orcid OAuth you can try on <a href='https://orcid.org/content/register-client-application-sandbox'>Sandbox Member API</a>."
        ),
        'clientId' => array(
            'type' => 'string',
            'label' => 'The Orcid client ID',
            'default' => "",
            'help' => "You must register your app at <a href='https://orcid.org/content/register-client-application'>Register a Client Application</a>."
        ),
        'clientSecret' => array(
            'type' => 'string',
            'label' => 'The Orcid client secret',
            'default' => '',
        ),
        'scope' => array(
            'type' => 'select',
            'label' => 'Scope',
            'options' => array(
                'authenticate' => 'authenticate',
                'read-limited' => 'read-limited',
            ),
            'default' => 'authenticate',
        ),
        'RegisterUri' => array(
            'type' => 'select',
            'label' => 'Register URI type',
            'options' => array(
                'OrcidAuthenticate' => 'OrcidAuthenticate',
                'AuthOauth' => 'AuthOauth',
            ),
            'default' => 'OrcidAuthenticate',
            'help' => "Current : %s"
        ),
        'AutomaticAttribute' => array(
            'type' => 'string',
            'label' => 'Orcid automatic attribute ',
            'default' => 'OrcidId',
            'help' => "Set description on any attribute directly activate this plugin (exept if disable in Surey management). Last name is used for name, email status for token, sent and validity date  for 1st access and reminder for the last access with Orcid Id."
        ),
        'OrcidIntroduction' => array(
            'type' => 'html',
            'label' => 'Orcid introduction',
            'default' => '[Organization name] is collecting your ORCID iD so we can [add purpose]. When you click the “Authorize” button, we will ask you to share your iD using an authenticated process: either by <a href="https://support.orcid.org/hc/articles/360006897454" target="_blank">registering for an ORCID iD</a> or, if you already have one, by <a href="https://support.orcid.org/hc/articles/360006971613" target="_blank">signing into your ORCID account</a>, then granting us permission to get your ORCID iD. We do this to ensure that you are correctly identified and securely connecting your ORCID iD. Learn more about <a href="https://orcid.org/blog/2017/02/20/whats-so-special-about-signing" target="_blank">What’s so special about signing in</a>.',
            'help' => "See <a href='https://members.orcid.org/api/resources/graphics#include-text'>Include text describing ORCID and a link to the ORCID website</a>. This was is used by default if survey didn‘t have one for selected language."
        ),
        'KeepOrcidId' => array(
            'type' => 'boolean',
            'label'=> 'Allow usage of same orcid id with different surey',
            'default' => 1,
        ),
    );

    /** @inheritdoc */
    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');

        $this->subscribe('beforeSurveyPage');

        $this->subscribe('getValidScreenFiles');

        $this->subscribe('newDirectRequest','OrcidOauthAuthenticate');
        $this->subscribe('newUnsecureRequest','OrcidOauthAuthenticate');

    }

    /** @inheritdoc */
    public function getPluginSettings($getValues = true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $PluginSettings = parent::getPluginSettings($getValues);
        $RegisterUriOrcidAuthenticate = Yii::app()->createAbsoluteUrl("plugins/direct", array('plugin' => 'OrcidAuthenticate','type'=>'register'));
        $RegisterUriAuthOauth = Yii::app()->createAbsoluteUrl("plugins/direct", array('plugin' => 'AuthOauth','client'=>'orcid','type'=>'register'));
        $currentRegisterUri = $this->get('RegisterUri',null,null,'OrcidAuthenticate');
        $PluginSettings['RegisterUri']['options']['OrcidAuthenticate'] = $RegisterUriOrcidAuthenticate;
        $PluginSettings['RegisterUri']['options']['AuthOauth'] = $RegisterUriAuthOauth;
        $PluginSettings['RegisterUri']['help'] = "Current : <input class='form-control form-control-static' type='text' readonly value='$RegisterUriOrcidAuthenticate'>";
        if($currentRegisterUri == "AuthOauth") {
            $PluginSettings['RegisterUri']['help'] = "Current : <input class='form-control form-control-static' type='text' readonly value='$RegisterUriAuthOauth'>";
        }
        return $PluginSettings;
    }
    /**
     * Activate or not
     */
    public function beforeActivate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (version_compare(Yii::app()->getConfig('versionnumber'),"3.13.0","<")) {
            $this->getEvent()->set('message', gT("Only for LimeSurvey 3.13.0 and up version"));
            $this->getEvent()->set('success', false);
        }
    }

    /**
     * Authentication via ORCID API (OAuth), redirect to surey after authnetication
     * @throw Exception
     * @return void
     */
    public function OrcidOauthAuthenticate()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $thisPlugin  = $this->getEvent()->get('target') == get_class($this);
        $OauthPlugin  = $this->getEvent()->get('target') == 'AuthOauth' && App()->getRequest()->getQuery('client') == 'orcid';

        if(!$thisPlugin && !$OauthPlugin) {
            return;
        }
        $surveyId = App()->session['currentOrcidSid'];
        $this->registerOrcidPackage();
        $provider = $this->getProvider();
        $scope = $this->get('scope',null,null,'authenticate');
        $authorizationUrl = $provider->getAuthorizationUrl(['scope'=>'/' . $scope]);
        if (!App()->getRequest()->getQuery('code')) {
            /* Global system ? Currently : must not : this an error */
            if(App()->getRequest()->getQuery('error')) {
                if($surveyId) {
                    $resetSurvey = App()->getController()->createUrl("/survey/index", array("sid" => $surveyId, "newtest" => "Y"));
                    App()->getController()->redirect($resetSurvey);
                }
            }
            throw new CHttpException(400,'Must use only for redirect uri');
        }
        if (!App()->getRequest()->getQuery('state')) {
            throw new CHttpException(400,'Invalid state');
        }
        if (App()->getRequest()->getQuery('state') != App()->session['oauthOrcid2state']) {
            throw new CHttpException(400,'Invalid state');
        }
        if(App()->getRequest()->getQuery('type') == 'register' && !App()->session['currentOrcidSid']) {
            throw new CHttpException(400,'Invalid state');
        }
        try {
            // Try to get an access token using the authorization code grant.
            $accessToken = $provider->getAccessToken('authorization_code', array(
                'code' => App()->getRequest()->getQuery('code')
            ));
            $orcidToken = $accessToken->getToken();
            if ($accessToken->hasExpired()) {
                throw new CHttpException(400,'Token was expired');
            }
            $sessionOrcidToken = array(
                'token' => $accessToken->getToken(),
                'refreshtoken' => $accessToken->getRefreshToken(),
                'expires' => $accessToken->getExpires(),
                'values' => $accessToken->getValues(),
            );
            App()->session['OrcidToken'] = $sessionOrcidToken;
            $this->gotoSurvey(App()->session['currentOrcidSid'],$sessionOrcidToken);
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            throw new CHttpException(400,$e->getMessage());
        }
    }

    /**
     * Create token and qhow the button to enter survey
     * @param integer $surveyId
     * @param array $orcidToken information
     * @param boolean $connected new connexion
     * @return void
     */
    private function gotoSurvey($surveyId, $aOrcidToken = null) {
        $orcidSurveyTokens = App()->session['orcidSurveyTokens'];
        if(is_null($orcidSurveyTokens)) {
            $orcidSurveyTokens = array();
        }
        if(empty($aOrcidToken)) {
            $aOrcidToken = App()->session['OrcidToken'];
        }
        if(empty($aOrcidToken)) {
            throw new CHttpException(500,"Invalid call of redirectToSurvey");
        }
        $OrcidIdAttribute = $this->getOrcidAttribute($surveyId);
        if(!$OrcidIdAttribute) {
            throw new CHttpException(500,"Invalid call of redirectToSurvey : no attribute set");
        }

        $orcidId = $aOrcidToken['values']['orcid'];
        $orcidToken = $aOrcidToken['token'];
        $orcidName = $aOrcidToken['values']['name'];
        $criteria = New CDbCriteria;
        $criteria->compare($OrcidIdAttribute,$orcidId);
        $oToken = Token::model($surveyId)->find($criteria);
        $dateTime = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i", Yii::app()->getConfig("timeadjust"));
        if(empty($oToken)) {
            $oToken = Token::create($surveyId);
            $oToken->setAttribute($OrcidIdAttribute,$orcidId);
            $oToken->setAttribute('lastname',$orcidName);
            $oToken->setAttribute('sent',$dateTime);
            $oToken->setAttribute('validfrom',$dateTime);
            $oToken->setAttribute('remindercount',0);
        } else {
            $oToken->setAttribute('remindersent',$dateTime);
            $oToken->remindercount++;
        }
        if(empty($oToken->token)) {
            $oToken->generateToken();
        }

        $oToken->setAttribute('emailstatus', $aOrcidToken['token']);
        $oToken->save(false);
        $orcidSurveyTokens[$surveyId] = $oToken->token;
        App()->session['orcidSurveyTokens'] = $orcidSurveyTokens;
        $enterSurvey = App()->getController()->createUrl("/survey/index", array("sid" => $surveyId, "token" => $oToken->token));
        App()->getController()->redirect($enterSurvey);
    }

    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get('surveyId');
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->translate('Orcid authentication'),
            'iconClass' => 'fa fa-user-circle',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $this->getEvent()->append('menuItems', array($menuItem));
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save'.get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }

        $aInputs = array(
            'OrcidAttribute',
            'ForceOrcidRegistering',
        );
        foreach($aInputs as $input) {
            $this->set(
                $input,
                App()->getRequest()->getPost($input),
                'Survey',
                $surveyId
            );
        }
        $aLangInputs = array(
            'OrcidIntroductions',
        );
        foreach($aLangInputs as $input) {
            $postedValues = array();
            foreach($oSurvey->getAllLanguages() as $lang) {
                $postedValues[$lang] = App()->getRequest()->getPost($input."_".$lang);
            }
            $this->set(
                $input,
                $postedValues,
                'Survey',
                $surveyId
            );
        }
        if (App()->getRequest()->getPost('save'.get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId));
            if (intval(App()->getConfig('versionnumber') <= 3)) {
                $redirectUrl = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
            }
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }

        $language = $oSurvey->language;
        $aSurveyLanguage = $oSurvey->getAllLanguages();
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            'Orcid Authentication settings' => $this->translate("Orcid Authentication settings"),
            'Introduction' => $this->translate("Introduction"),
            'Introduction text in %s (%s)' => $this->translate("Introduction text in %s (%s)"),
            'Default introduction' => $this->translate("Default introduction"),
        );
        $aData['errors'] = array();

        /* @var array[] aSettings for SettingsWidget */
        $aSettings = array();

        /* Orcid attribute */
        $aTokens = array(
            'firstname'=>gT("First name"),
            //~ 'lastname'=>gT("Last name"),
            'emailstatus'=>gT("Email status"),
        );
        foreach($oSurvey->getTokenAttributes() as $attribute=>$information) {
            $aTokens[$attribute] = empty($information['description']) ? $attribute : $information['description'];
        }
        $aTokens['none'] = $this->gT("None (disable)");
        $defaultAttribute = $this->get("AutomaticAttribute", null, null, "OrcidId");
        $emptyString = $this->gt("Leave default (none)");
        if ($defaultAttribute) {
            $emptyString = sprintf($this->gt("Leave default : %s (no set in this survey)"),$defaultAttribute);
            if(in_array($defaultAttribute,$aTokens)) {
                $emptyString = sprintf($this->gt("Leave default : %s"),$defaultAttribute);
            }
        }
        $forceOrcidRegistering = $this->get('forceOrcidRegistering','Survey',$surveyId,"auto");
        if(strval($forceOrcidRegistering) === "1") {
            $forceOrcidRegistering = 'always';
        } elseif(strval($forceOrcidRegistering) === "0") {
            $forceOrcidRegistering = 'never';
        }
        $aOrcidSettings = array(
            'OrcidAttribute'=>array(
                'type'=>'select',
                'options'=> $aTokens,
                'htmlOptions' => array(
                    'empty' => $emptyString
                ),
                'label'=>$this->gT('Use orcid registering'),
                //'help' => $this->gT('Lastname receive the orcid name if set'),
                'current'=>$this->get('OrcidAttribute','Survey',$surveyId,'')
            ),
            'ForceOrcidRegistering'=>array(
                'type' => 'select',
                'options'=> array(
                    'never' => $this->gT("Never"),
                    'auto' => $this->gT("Automatic"),
                    'always' => $this->gT("Always"),
                ),
                'label' => $this->gT('Force orcid authenticate'),
                'current' => $forceOrcidRegistering,
                'help' => $this->gT("Always totally disable usage of single token, only allowed to enter in survey after orcid authentication. If token is allowed : it can come from url and a the form show an password input. With automatic : if token already exist and have an Orcid-id : user must authenticate to Orcid to use the token."),
            )
        );
        $aSettings[$this->translate("Orcid activation")] = $aOrcidSettings;
        $aOrcidIntroduction = array();
        $contentIntroduction = "";
        $aOrcidIntroductions = $this->get('OrcidIntroductions','Survey',$surveyId,array());
        $sOrcidDefault = $this->get('OrcidIntroduction',null,null,$this->settings['OrcidIntroduction']['default']);
        $aData['sOrcidDefault'] = $sOrcidDefault;
        $contentIntroduction .= $this->renderPartial('admin.DefaultIntroductionSetting', array_merge($aData), true);
        foreach($aSurveyLanguage as $languageCode) {
            $sOrcidIntroduction = isset($aOrcidIntroductions[$languageCode]) ? trim($aOrcidIntroductions[$languageCode]) : "";
            $aIntroductionContentData = array(
                'name' => "OrcidIntroductions_{$languageCode}",
                'placeholder'=>$sOrcidDefault,
                'value' => $sOrcidIntroduction,
                'languagecode' => $languageCode,
                'languageDetail' => getLanguageDetails($languageCode),
            );
            $contentIntroduction .= $this->renderPartial('admin.IntroductionSetting', array_merge($aData,$aIntroductionContentData), true);
        }
        $aSettings[$this->translate("Orcid Introduction")] = array(
            'OrcidIntroduction'=>array(
                'type'=>'info',
                'content'=> $contentIntroduction,
            ),
        );

        $aData['aSettings']=$aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId)),
        );
        if(empty($this->get("clientId")) || empty($this->get("clientSecret"))) {
            $aData['errors'][] = $this->gT("Unable to use orcid authentication without clientId or clientSecret. Please review configuration of the plugin.");
        }
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }

    /**
     * see beforeSurveyPage
     */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if(empty($this->get("clientId")) || empty($this->get("clientSecret"))) {
            return;
        }
        $iSurveyId = $this->getEvent()->get('surveyId');
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        if(!$oSurvey) {
            return;
        }
        if (App()->getRequest()->getParam('action') == 'previewgroup' || App()->getRequest()->getParam('action') == 'previewquestion') {
            return;
        }
        if ( App()->getRequest()->getParam('clearall') == 'clearall' && 
            ( App()->getRequest()->getPost('confirm-clearall') || App()->getRequest()->getParam('orcid-logout') == 'logout')
        ) {
            App()->session['orcidSurveyTokens'] = null;
            App()->session['OrcidToken'] = null;
            $urlLogout = 'https://orcid.org/userStatus.json?logUserOut=true';
            if($this->get('SandBox')) {
                $urlLogout = 'https://sandbox.orcid.org/userStatus.json?logUserOut=true';
            }
            $this->registerOrcidPackage();
            $scriptLogOut = "orcidLogout('{$urlLogout}');";
            App()->getClientScript()->registerScript("orcidScriptLogOut",$scriptLogOut);
            return;
        }

        if (App()->getRequest()->getParam('newtest') == 'Y') {
            $orcidSurveyTokens = App()->session['orcidSurveyTokens'];
            unset($orcidSurveyTokens[$iSurveyId]);
            App()->session['orcidSurveyTokens'] = $orcidSurveyTokens;
        }
        if($oSurvey->active != "Y") {
            return;
        }
        if (!$oSurvey->getHasTokensTable()) {
            return;
        }
        $OrcidAttribute = $this->getOrcidAttribute($iSurveyId);
        if(!$OrcidAttribute) {
            return;
        }

        $forced = $this->get('ForceOrcidRegistering','Survey',$iSurveyId);
        $forceOrcidRegistering = $this->get('forceOrcidRegistering','Survey',$iSurveyId,"auto");
        if(strval($forceOrcidRegistering) === "1") {
            $forceOrcidRegistering = 'always';
        } elseif(strval($forceOrcidRegistering) === "0") {
            $forceOrcidRegistering = 'never';
        }
        $token = App()->getRequest()->getParam('token');
        $tokenSession = isset($_SESSION['survey_'.$iSurveyId]['token']) ? $_SESSION['survey_'.$iSurveyId]['token'] : null;
        if(empty($token)) {
            $token = $tokenSession;
        }
        /* If session is already started but different than param : must delete */
        if ($tokenSession && $token != $tokenSession) {
            unset($tokenSession);
            $orcidSurveyTokens = App()->session['orcidSurveyTokens'];
            unset($orcidSurveyTokens[$iSurveyId]);
            App()->session['orcidSurveyTokens'] = $orcidSurveyTokens;
        }
        $orcidSurveyTokens = App()->session['orcidSurveyTokens'];
        $orcidSurveyToken = empty($orcidSurveyTokens[$iSurveyId]) ? null : $orcidSurveyTokens[$iSurveyId];
        if ($orcidSurveyToken) {
            /* OK : we are connected to this survey with this token : always valid */
            if ($orcidSurveyToken == $token) {
                return;
            }
        }
        if ($forceOrcidRegistering == "never" && $token) {
            /* No need to check */
            return;
        }
        if ($forceOrcidRegistering == "always" || !$token) {
            /* Start showing */
            $this->showOrcidTokenForm($iSurveyId);
            return;
        }

        /* we know $forceOrcidRegistering == "" and we have a token */
        $oToken = Token::model($iSurveyId)->findByToken($token);
        if(empty($oToken)) {
            return;
        }
        if(empty($oToken->$OrcidAttribute)) {
            return;
        }
        /* Start showing */
        $this->showOrcidTokenForm($iSurveyId);

    }

    public function getPluginTwigPath()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $viewPath = dirname(__FILE__)."/twig/add";
        $this->getEvent()->append('add', array($viewPath));
    }

    public function getValidScreenFiles()
    {
        if (intval(App()->getConfig('versionnumber') > 3)) {
            // getValidScreenFiles seems broken in 5.X @TODO : report issue
            return;
        }
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->subscribe('getPluginTwigPath');
        if (
            $this->getEvent()->get("type") != 'view' ||
            ( $this->getEvent()->get("screen") && $this->getEvent()->get("screen") != "register" )
        ) {
            return;
        }
        $this->getEvent()->append('add', array(
            "subviews".DIRECTORY_SEPARATOR."logincomponents".DIRECTORY_SEPARATOR."token_orcid.twig",
            "subviews".DIRECTORY_SEPARATOR."logincomponents".DIRECTORY_SEPARATOR."orcid_description.twig",
            "subviews".DIRECTORY_SEPARATOR."logincomponents".DIRECTORY_SEPARATOR."orcid_button.twig",
            "subviews".DIRECTORY_SEPARATOR."logincomponents".DIRECTORY_SEPARATOR."orcid_registerbutton.twig",
        ));
    }

    /**
     * @inheritdoc
     * With default escape mode to 'unescaped'
     */
    public function gT($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($sToTranslate, $sEscapeMode, $sLanguage);
    }

    /**
     * @inheritdoc

     * @return void
     */
    public function log($message, $level = \CLogger::LEVEL_TRACE, $logDetail = null)
    {
        if(!$logDetail && $this->getEvent()) {
            $logDetail = $this->getEvent()->getEventName();
        } // What to put if no event ?
        Yii::log($message, $level,$this->getName().".".$logDetail);
    }

    /**
     * Get the current  $provider
     * @param string $state : register/authenticate. Current only register
     * @return false|\League\OAuth2\Client\Provider\GenericProvider
     */
    private function getProvider($case = 'register')
    {
        require_once __DIR__ . '/vendor/autoload.php';
        if($this->provider) {
            return $this->provider;
        }
        $clientId = $this->get('clientId');
        if(empty($clientId)) {
            return false;
        }
        $clientSecret = $this->get('clientSecret');
        if(empty($clientSecret)) {
            return false;
        }
        $urlAuthorize = 'https://orcid.org/oauth/authorize';
        $urlAccessToken = 'https://orcid.org/oauth/token';
        if($this->get('SandBox')) {
            $urlAuthorize = 'https://sandbox.orcid.org/oauth/authorize';
            $urlAccessToken = 'https://sandbox.orcid.org/oauth/token';
        }
        /* case = register */
        $currentRegisterUri = $this->get('RegisterUri',null,null,'OrcidAuthenticate');
        $RedirectUri = Yii::app()->createAbsoluteUrl("plugins/direct", array('plugin' => 'OrcidAuthenticate','type'=>'register'));;
        if($currentRegisterUri == "AuthOauth") {
           $RedirectUri = Yii::app()->createAbsoluteUrl("plugins/direct", array('plugin' => 'AuthOauth','client'=>'orcid','type'=>'register'));
        }
        $providerConfig = array(
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'redirectUri' => $RedirectUri,
            'urlAuthorize' => $urlAuthorize,
            'urlAccessToken' => $urlAccessToken,
            'urlResourceOwnerDetails' => $urlAccessToken
        );
        if (App()->getConfig('proxy_host_name')) {
            $providerConfig['proxy'] = App()->getConfig('proxy_host_name').":".App()->getConfig('proxy_host_port');
            if (!is_null(App()->getConfig('proxy_host_verify'))) {
                $providerConfig['verify'] = App()->getConfig('proxy_host_verify');
            }
        }
        $this->provider = new \League\OAuth2\Client\Provider\GenericProvider($providerConfig);
        return $this->provider;
    }

    /**
     * Get the attribute for a survey
     * @param int $surveyId
     * return false|string
     */
    private function getOrcidAttribute($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aAttribute = $oSurvey->getTokenAttributes();
        $OrcidAttribute = $this->get('OrcidAttribute','Survey',$surveyId,'');
        if($OrcidAttribute == 'none') {
            return false;
        }
        if(empty($OrcidAttribute) ) {
            $AutomaticAttribute = $this->get('AutomaticAttribute',null,null,'OrcidId');
            if(empty($AutomaticAttribute)) {
                return false;
            }
            foreach($aAttribute as $attribute => $info) {
                if($info['description'] == $AutomaticAttribute) {
                    $OrcidAttribute = $attribute;
                    break;
                }
                $this->log("Attribute $AutomaticAttribute not found in survey $surveyId",'info');
                return false;
            }
        } else {
            $aAvailableAttribute =array_merge(array('firstname','emailstatus'),array_keys($aAttribute));
            if (!in_array($OrcidAttribute,$aAvailableAttribute)) {
                $this->log("Invalid attribute $OrcidAttribute set in survey $iSurveyId",'error');
                return false;
            }
        }
        return $OrcidAttribute;
    }

    /**
     * Get current OrcidIntroduction with current language
     * @param $sureyId
     * @param $language
     * @return string
     */
    private function getOrcidIntroduction($surveyId, $language)
    {
        $OrcidIntroductions = $this->get('OrcidIntroductions','Survey',$surveyId,array());
        $surveyIntroduction = isset($OrcidIntroductions[$language]) ? trim($OrcidIntroductions[$language]) : null;
        if($surveyIntroduction) {
            return $surveyIntroduction;
        }
        return $this->get('OrcidIntroduction');
    }

    /**
     * Show the orcid connect for current survey
     * @param integer $surveyId
     * @return void
     */
    private function showOrcidTokenForm($iSurveyId)
    {
        $aOrcidToken = App()->session['OrcidToken'];
        if($this->get('KeepOrcidId', null, null, 1)) {
            if ($aOrcidToken && isset($aOrcidToken['expires']) && $aOrcidToken['expires'] >= time())  {
                // See League\OAuth2\Client\Token\hasExpired
                $this->showOrcidTokenConfirm($iSurveyId);
            }
        }
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        $language = App()->getLanguage();
        if(!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        $this->subscribe('getPluginTwigPath');
        $provider = $this->getProvider();
        $scope = $this->get('scope',null,null,'authenticate');
        $authorizationUrl = $provider->getAuthorizationUrl(['scope'=>'/' . $scope, 'lang'=>$language]);
        App()->session['oauthOrcid2state'] = $provider->getState();
        App()->session['currentOrcidSid'] = $iSurveyId;
        $aRenderData = array();
        $renderData['aSurveyInfo'] = getSurveyInfo($iSurveyId,$language);
        $renderData['aSurveyInfo']['surveyUrl'] = App()->createUrl("survey/index",array('sid'=>$iSurveyId));
        if($renderData['aSurveyInfo']['additional_languages']) {
            $renderData['aSurveyInfo']['alanguageChanger']['show']  = true;
            $renderData['aSurveyInfo']['alanguageChanger']['datas'] = getLanguageChangerDatas($language);
        }
        $renderData['aSurveyInfo']['include_content'] = 'userforms';
        $renderData['aSurveyInfo']['showprogress'] = false;
        $renderData['aSurveyInfo']['aForm'] = array(
            'sType' => 'token_orcid'
        );
        $renderData['aSurveyInfo']['orcid'] = array(
            'forced' => $this->get('ForceOrcidRegistering','Survey',$iSurveyId),
            'introduction' => $this->getOrcidIntroduction($iSurveyId,$language),
            'authorizationUrl' => $authorizationUrl,
            'registerUrl' => App()->createUrl("register/index",array('sid'=>$iSurveyId,'lang'=>$language)),
            'lang' => array(
                'titleonlyorcid' => $this->translate("To participate in this restricted survey, you need a authenticate on ORCID."),
                'titlewithtoken' => $this->translate("To participate in this restricted survey, you need a valid token or to authenticate on ORCID."),
                'button' => $this->translate('Register or Connect your ORCID iD'),
                'registerbutton' => $this->translate('Register with your email.'),
                'or' => $this->translate('or'),
                'imgalt' => $this->translate('ORCID iD icon'),
                'introduction' => sprintf(
                    $this->translate('When you click the “Authorize” button, we will ask you to share your iD using an authenticated process: either by %sregistering for an ORCID iD%s or, if you already have one, by %ssigning into your ORCID account%s , then granting us permission to get your ORCID iD. We do this to ensure that you are correctly identified and securely connecting your ORCID iD. Learn more about %sWhat’s so special about signing in%s.'),
                    '<a href="https://support.orcid.org/hc/articles/360006897454" target="_blank">',
                    '</a>',
                    '<a href="https://support.orcid.org/hc/articles/360006971613" target="_blank">',
                    '</a>',
                    '<a href="https://orcid.org/blog/2017/02/20/whats-so-special-about-signing" target="_blank">',
                    '</a>'
                ),
            ),
        );
        App()->clientScript->registerScriptFile(Yii::app()->getConfig("generalscripts").'nojs.js', CClientScript::POS_HEAD);
        $this->registerOrcidPackage();
        Template::model()->getInstance(null, $iSurveyId);
        Yii::app()->twigRenderer->renderTemplateFromFile('layout_global.twig', $renderData, false);
        //Yii::app()->end();
    }

    /**
     * Show the orcid connect for current survey
     * @param integer $surveyId
     * @return void
     */
    private function showOrcidTokenConfirm($iSurveyId)
    {
        if(Yii::app()->getRequest()->getPost('confirm') == 'confirm') {
            $this->gotoSurvey($iSurveyId);
        }
        if(Yii::app()->getRequest()->getPost('logout') == 'logout') {
            App()->session['OrcidToken'] = null;
            $urlLogout = 'https://orcid.org/userStatus.json?logUserOut=true';
            if($this->get('SandBox')) {
                $urlLogout = 'https://sandbox.orcid.org/userStatus.json?logUserOut=true';
            }
            $scriptLogOut = " orcidLogout('{$urlLogout}');";
            App()->getClientScript()->registerScript("orcidScriptLogOut",$scriptLogOut);
            return;
        }
        $this->subscribe('getPluginTwigPath');
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        $language = App()->getLanguage();
        if(!in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        $aOrcidToken = App()->session['OrcidToken'];
        App()->session['currentOrcidSid'] = $iSurveyId;
        $aRenderData = array();
        $renderData['aSurveyInfo'] = getSurveyInfo($iSurveyId,$language);
        $renderData['aSurveyInfo']['include_content'] = 'userforms';
        $renderData['aSurveyInfo']['showprogress'] = false;
        $renderData['aSurveyInfo']['aForm'] = array(
            'sType' => 'confirm_orcid'
        );
        $renderData['aSurveyInfo']['orcid'] = array(
            'introduction' => $this->getOrcidIntroduction($iSurveyId,$language),
            'orcid-id' => $this->get('OrcidIntroduction'),
            'lang' => array(
                'title' =>  $this->translate("You can use your current Orcid ID to enter this survey."),
                'confirm' => $this->translate("Enter this survey with you ORCID ID"),
                'logout' => $this->translate("Log out to ORCID and enter in survey."),
            ),
        );
        $this->registerOrcidPackage();
        Template::model()->getInstance(null, $iSurveyId);
        Yii::app()->twigRenderer->renderTemplateFromFile('layout_global.twig', $renderData, false);
        Yii::app()->end();
    }

    /**
     * Create and register Orcid package
     * @see https://members.orcid.org/api/resources/graphics
     * @return void
     */
    private function registerOrcidPackage()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $min = (App()->getConfig('debug')) ? '.min' : '';

        if(!Yii::app()->clientScript->hasPackage('OrcidPractice')) { 
            Yii::app()->clientScript->addPackage('OrcidPractice', array(
                'basePath'    => get_class($this).'.assets',
                'js'          => array('OrcidAuthenticate.js'),
                'css'          => array('OrcidAuthenticate.css'),
                'depends'      =>array('jquery'),
            ));
        }
        /* Registering the package */
        Yii::app()->getClientScript()->registerPackage('OrcidPractice');
    }
    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable(array($this, 'gT'))) {
            return $this->gT($string, $sEscapeMode, $sLanguage);
        }
        return $string;
    }
}
