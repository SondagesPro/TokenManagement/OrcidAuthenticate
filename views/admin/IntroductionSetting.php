<?php
Yii::import('application.helpers.admin.htmleditor_helper', true);
PrepareEditorScript(true);
?>

<div class="form-group">
    <label class="default control-label col-sm-6" for="<?php echo CHtml::getIdByName($name); ?>">
        <?php printf($lang['Introduction text in %s (%s)'],$languageDetail['description'],$languagecode); ?>
    </label>
    <div class="col-sm-6 controls">
        <div style="height:auto;width:100%" class="well">
            <div class="htmleditor input-group">
            <?php
            echo CHtml::textArea($name,$value,array("class"=>"form-control","rows"=>8, 'id' => $name));
            echo getEditor("editdescription_".$languagecode,$name, "[Orcid](".$languagecode.")", $surveyId, 0, 0, 'surveygeneralsettings');
            ?>
            </div>
        </div>
    </div>
</div>
